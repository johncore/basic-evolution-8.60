-- Intoxicate Target by Arthur, aka artofwork
-- This is an attack spell similar to exori vis
-- this will inflict the target player/creature with
-- a drunk condition that last for 1 minute

local combat = createCombatObject()
setCombatParam(combat, COMBAT_PARAM_EFFECT, CONST_ME_MAGIC_RED)
setCombatParam(combat, COMBAT_PARAM_DISTANCEEFFECT, CONST_ANI_WEAPONTYPE)
setCombatParam(combat, COMBAT_PARAM_AGGRESSIVE, TRUE)
setCombatParam(combat, COMBAT_PARAM_USECHARGES, true)
setCombatParam(combat, COMBAT_PARAM_BLOCKARMOR, true)

local condition = createConditionObject(CONDITION_DRUNK)
setConditionParam(condition, CONDITION_PARAM_TICKS, 30 * 1000)
setConditionParam(condition, CONDITION_PARAM_BUFF, TRUE)
setCombatCondition(combat, condition)

function onGetFormulaValues(cid, level, skill, attack, factor)
	local skillTotal, levelTotal = skill + attack, level / 3
	return -(skillTotal / 2 + levelTotal), - (skillTotal + levelTotal)
end

-- remove unnecessary conditions
setCombatCallback(combat, CALLBACK_PARAM_SKILLVALUE, "onGetFormulaValues")
function onCastSpell(cid, var)
          return doCombat(cid, combat, var)
end
