function onCastSpell(cid, var)
local left = getPlayerSlotItem(cid, 6)
local right = getPlayerSlotItem(cid, 5)
        if left.itemid == 7384 then
				doItemSetAttribute(left.uid, "aid", 12001)
				doItemSetAttribute(left.uid, "description", "It is enhanced with fire magic.")
                doSendMagicEffect(getCreaturePosition(cid), CONST_ME_FIREATTACK)
        elseif right.itemid == 7384 then
				doItemSetAttribute(right.uid, "aid", 12001)
				doItemSetAttribute(right.uid, "description", "It is enhanced with fire magic.")
                doSendMagicEffect(getCreaturePosition(cid), CONST_ME_FIREATTACK)
        else
                doPlayerSendCancel(cid, "Dejavu Sword must be on your hands to cast this spell.")
        end
return true
end