local combat1 = createCombatObject() 
setCombatParam(combat1, COMBAT_PARAM_TYPE, COMBAT_ENERGYDAMAGE) 
setCombatParam(combat1, COMBAT_PARAM_EFFECT, 7) 
setCombatFormula(combat1, COMBAT_FORMULA_LEVELMAGIC, -0.0, -0, -0, -0) 
local combat2 = createCombatObject() 
setCombatParam(combat2, COMBAT_PARAM_TYPE, COMBAT_ENERGYDAMAGE) 
setCombatParam(combat2, COMBAT_PARAM_EFFECT, 7) 
setCombatFormula(combat2, COMBAT_FORMULA_LEVELMAGIC, -0.0, -0, -0, -0) 
local combat3 = createCombatObject() 
setCombatParam(combat3, COMBAT_PARAM_TYPE, COMBAT_ENERGYDAMAGE) 
setCombatParam(combat3, COMBAT_PARAM_EFFECT, 7) 
setCombatFormula(combat3, COMBAT_FORMULA_LEVELMAGIC, -0.0, -0, -0, -0) 
local combat4 = createCombatObject() 
setCombatParam(combat4, COMBAT_PARAM_TYPE, COMBAT_ENERGYDAMAGE) 
setCombatParam(combat4, COMBAT_PARAM_EFFECT, 7) 
setCombatFormula(combat4, COMBAT_FORMULA_LEVELMAGIC, -0.0, -0, -0, -0) 
local combat5 = createCombatObject() 
setCombatParam(combat5, COMBAT_PARAM_TYPE, COMBAT_ENERGYDAMAGE) 
setCombatParam(combat5, COMBAT_PARAM_EFFECT, 7) 
setCombatFormula(combat5, COMBAT_FORMULA_LEVELMAGIC, -0.0, -0, -0, -0) 
local combat6 = createCombatObject() 
setCombatParam(combat6, COMBAT_PARAM_TYPE, COMBAT_ENERGYDAMAGE) 
setCombatParam(combat6, COMBAT_PARAM_EFFECT, 48) 
setCombatFormula(combat6, COMBAT_FORMULA_LEVELMAGIC, -1.3, -54, -1, -60) 
local combat7 = createCombatObject() 
setCombatParam(combat7, COMBAT_PARAM_TYPE, COMBAT_ENERGYDAMAGE) 
setCombatParam(combat7, COMBAT_PARAM_EFFECT, 48) 
setCombatFormula(combat7, COMBAT_FORMULA_LEVELMAGIC, -1.3, -54, -1, -60) 
local combat8 = createCombatObject() 
setCombatParam(combat8, COMBAT_PARAM_TYPE, COMBAT_ENERGYDAMAGE) 
setCombatParam(combat8, COMBAT_PARAM_EFFECT, 48) 
setCombatFormula(combat8, COMBAT_FORMULA_LEVELMAGIC, -1.3, -54, -1, -60) 
local combat9 = createCombatObject() 
setCombatParam(combat9, COMBAT_PARAM_TYPE, COMBAT_ENERGYDAMAGE) 
setCombatParam(combat9, COMBAT_PARAM_EFFECT, 48) 
setCombatFormula(combat9, COMBAT_FORMULA_LEVELMAGIC, -1.3, -54, -1, -60) 
local combat10 = createCombatObject() 
setCombatParam(combat10, COMBAT_PARAM_TYPE, COMBAT_ENERGYDAMAGE) 
setCombatParam(combat10, COMBAT_PARAM_EFFECT, 48) 
setCombatFormula(combat10, COMBAT_FORMULA_LEVELMAGIC, -1.3, -54, -1, -60) 
local combat11 = createCombatObject() 
setCombatParam(combat11, COMBAT_PARAM_TYPE, COMBAT_ENERGYDAMAGE) 
setCombatParam(combat11, COMBAT_PARAM_EFFECT, 48) 
setCombatFormula(combat11, COMBAT_FORMULA_LEVELMAGIC, -1.3, -54, -1, -60) 
local combat12 = createCombatObject() 
setCombatParam(combat12, COMBAT_PARAM_TYPE, COMBAT_ENERGYDAMAGE) 
setCombatParam(combat12, COMBAT_PARAM_EFFECT, 48) 
setCombatFormula(combat12, COMBAT_FORMULA_LEVELMAGIC, -1.3, -84, -1.6, -102) 
local combat13 = createCombatObject() 
setCombatParam(combat13, COMBAT_PARAM_TYPE, COMBAT_ENERGYDAMAGE) 
setCombatParam(combat13, COMBAT_PARAM_EFFECT, 48) 
setCombatFormula(combat13, COMBAT_FORMULA_LEVELMAGIC, -1.3, -84, -1.6, -102) 
local combat14 = createCombatObject() 
setCombatParam(combat14, COMBAT_PARAM_TYPE, COMBAT_ENERGYDAMAGE) 
setCombatParam(combat14, COMBAT_PARAM_EFFECT, 48) 
setCombatFormula(combat14, COMBAT_FORMULA_LEVELMAGIC, -1.3, -84, -1.6, -102) 
local tcombat1 = createCombatObject() 
setCombatParam(tcombat1, COMBAT_PARAM_TYPE, COMBAT_DEATHDAMAGE) 
setCombatParam(tcombat1, COMBAT_PARAM_EFFECT, 48) 
setCombatFormula(tcombat1, COMBAT_FORMULA_LEVELMAGIC, -0.0, -0, -0, -0) 
local tcombat2 = createCombatObject() 
setCombatParam(tcombat2, COMBAT_PARAM_TYPE, COMBAT_DEATHDAMAGE) 
setCombatParam(tcombat2, COMBAT_PARAM_EFFECT, 48) 
setCombatFormula(tcombat2, COMBAT_FORMULA_LEVELMAGIC, -0.0, -0, -0, -0) 
local tcombat3 = createCombatObject() 
setCombatParam(tcombat3, COMBAT_PARAM_TYPE, COMBAT_DEATHDAMAGE) 
setCombatParam(tcombat3, COMBAT_PARAM_EFFECT, 48) 
setCombatFormula(tcombat3, COMBAT_FORMULA_LEVELMAGIC, -0.0, -0, -0, -0) 
local tcombat4 = createCombatObject() 
setCombatParam(tcombat4, COMBAT_PARAM_TYPE, COMBAT_DEATHDAMAGE) 
setCombatParam(tcombat4, COMBAT_PARAM_EFFECT, 48) 
setCombatFormula(tcombat4, COMBAT_FORMULA_LEVELMAGIC, -0.0, -0, -0, -0) 
local tcombat5 = createCombatObject() 
setCombatParam(tcombat5, COMBAT_PARAM_TYPE, COMBAT_DEATHDAMAGE) 
setCombatParam(tcombat5, COMBAT_PARAM_EFFECT, 28) 
setCombatFormula(tcombat5, COMBAT_FORMULA_LEVELMAGIC, -0.0, -0, -0, -0) 
local tcombat6 = createCombatObject() 
setCombatParam(tcombat6, COMBAT_PARAM_TYPE, COMBAT_DEATHDAMAGE) 
setCombatParam(tcombat6, COMBAT_PARAM_EFFECT, 28) 
setCombatFormula(tcombat6, COMBAT_FORMULA_LEVELMAGIC, -1.3, -54, -1, -60) 
local tcombat7 = createCombatObject() 
setCombatParam(tcombat7, COMBAT_PARAM_TYPE, COMBAT_DEATHDAMAGE) 
setCombatParam(tcombat7, COMBAT_PARAM_EFFECT, 28) 
setCombatFormula(tcombat7, COMBAT_FORMULA_LEVELMAGIC, -1.3, -54, -1, -60) 
local tcombat8 = createCombatObject() 
setCombatParam(tcombat8, COMBAT_PARAM_TYPE, COMBAT_DEATHDAMAGE) 
setCombatParam(tcombat8, COMBAT_PARAM_EFFECT, 28) 
setCombatFormula(tcombat8, COMBAT_FORMULA_LEVELMAGIC, -1.3, -54, -1, -60) 
local tcombat9 = createCombatObject() 
setCombatParam(tcombat9, COMBAT_PARAM_TYPE, COMBAT_DEATHDAMAGE) 
setCombatParam(tcombat9, COMBAT_PARAM_EFFECT, 28) 
setCombatFormula(tcombat9, COMBAT_FORMULA_LEVELMAGIC, -1.3, -54, -1, -60) 
local tcombat10 = createCombatObject() 
setCombatParam(tcombat10, COMBAT_PARAM_TYPE, COMBAT_DEATHDAMAGE) 
setCombatParam(tcombat10, COMBAT_PARAM_EFFECT, 28) 
setCombatFormula(tcombat10, COMBAT_FORMULA_LEVELMAGIC, -1.3, -54, -1, -60) 
local tcombat11 = createCombatObject() 
setCombatParam(tcombat11, COMBAT_PARAM_TYPE, COMBAT_DEATHDAMAGE) 
setCombatParam(tcombat11, COMBAT_PARAM_EFFECT, 28) 
setCombatFormula(tcombat11, COMBAT_FORMULA_LEVELMAGIC, -1.3, -54, -1, -60) 
local tcombat12 = createCombatObject() 
setCombatParam(tcombat12, COMBAT_PARAM_TYPE, COMBAT_DEATHDAMAGE) 
setCombatParam(tcombat12, COMBAT_PARAM_EFFECT, 39) 
setCombatFormula(tcombat12, COMBAT_FORMULA_LEVELMAGIC, -1.3, -84, -1.6, -102) 
local tcombat13 = createCombatObject() 
setCombatParam(tcombat13, COMBAT_PARAM_TYPE, COMBAT_DEATHDAMAGE) 
setCombatParam(tcombat13, COMBAT_PARAM_EFFECT, 39) 
setCombatFormula(tcombat13, COMBAT_FORMULA_LEVELMAGIC, -1.3, -84, -1.6, -102) 
local tcombat14 = createCombatObject() 
setCombatParam(tcombat14, COMBAT_PARAM_TYPE, COMBAT_DEATHDAMAGE) 
setCombatParam(tcombat14, COMBAT_PARAM_EFFECT, 39) 
setCombatFormula(tcombat14, COMBAT_FORMULA_LEVELMAGIC, -1.3, -84, -1.6, -102) 
arr1 = { 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} 
} 
arr2 = { 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 1, 3, 1, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} 
} 
arr3 = { 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0}, 
{0, 0, 0, 0, 1, 3, 1, 0, 0, 0, 0}, 
{0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} 
} 
arr4 = { 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0}, 
{0, 0, 0, 1, 1, 3, 1, 1, 0, 0, 0}, 
{0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} 
} 
arr5 = { 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0}, 
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}, 
{0, 0, 0, 1, 1, 3, 1, 1, 0, 0, 0}, 
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}, 
{0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0} 
} 
arr6 = { 
{0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0}, 
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}, 
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}, 
{0, 0, 0, 1, 1, 3, 1, 1, 0, 0, 0}, 
{0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} 
} 
arr7 = { 
{0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0}, 
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}, 
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}, 
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}, 
{0, 0, 0, 0, 1, 3, 1, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} 
} 
arr8 = { 
{0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0}, 
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}, 
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}, 
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}, 
{0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} 
} 
arr9 = { 
{0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0}, 
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}, 
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}, 
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}, 
{0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} 
} 
arr10 = { 
{0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0}, 
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}, 
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}, 
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}, 
{0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} 
} 
arr11 = { 
{0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0}, 
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}, 
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}, 
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}, 
{0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} 
} 
arr12 = { 
{0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} 
} 
arr13 = { 
{0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0}, 
{0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0}, 
{0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0}, 
{0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0}, 
{0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0}, 
{0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0}, 
{0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0}, 
{0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0}, 
{0, 0, 0, 0, 1, 2, 1, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} 
} 
arr14 = { 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0}, 
{0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0}, 
{0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0}, 
{0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0}, 
{0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0}, 
{0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} 
} 
local area1 = createCombatArea(arr1) 
local area2 = createCombatArea(arr2) 
local area3 = createCombatArea(arr3) 
local area4 = createCombatArea(arr4) 
local area5 = createCombatArea(arr5) 
local area6 = createCombatArea(arr6) 
local area7 = createCombatArea(arr7) 
local area8 = createCombatArea(arr8) 
local area9 = createCombatArea(arr9) 
local area10 = createCombatArea(arr10) 
local area11 = createCombatArea(arr11) 
local area12 = createCombatArea(arr12) 
local area13 = createCombatArea(arr13) 
local area14 = createCombatArea(arr14) 
setCombatArea(combat1, area1) 
setCombatArea(combat2, area2) 
setCombatArea(combat3, area3) 
setCombatArea(combat4, area4) 
setCombatArea(combat5, area5) 
setCombatArea(combat6, area6) 
setCombatArea(combat7, area7) 
setCombatArea(combat8, area8) 
setCombatArea(combat9, area9) 
setCombatArea(combat10, area10) 
setCombatArea(combat11, area11) 
setCombatArea(combat12, area12) 
setCombatArea(combat13, area13) 
setCombatArea(combat14, area14) 
setCombatArea(tcombat1, area1) 
setCombatArea(tcombat2, area2) 
setCombatArea(tcombat3, area3) 
setCombatArea(tcombat4, area4) 
setCombatArea(tcombat5, area5) 
setCombatArea(tcombat6, area6) 
setCombatArea(tcombat7, area7) 
setCombatArea(tcombat8, area8) 
setCombatArea(tcombat9, area9) 
setCombatArea(tcombat10, area10) 
setCombatArea(tcombat11, area11) 
setCombatArea(tcombat12, area12) 
setCombatArea(tcombat13, area13) 
setCombatArea(tcombat14, area14) 
local function onCastSpell1(parameters) 
    doCombat(parameters.cid, combat1, parameters.var) 
end 
local function onCastSpell2(parameters) 
    doCombat(parameters.cid, combat2, parameters.var) 
end 
local function onCastSpell3(parameters) 
    doCombat(parameters.cid, combat3, parameters.var) 
end 
local function onCastSpell4(parameters) 
    doCombat(parameters.cid, combat4, parameters.var) 
end 
local function onCastSpell5(parameters) 
    doCombat(parameters.cid, combat5, parameters.var) 
end 
local function onCastSpell6(parameters) 
    doCombat(parameters.cid, combat6, parameters.var) 
end 
local function onCastSpell7(parameters) 
    doCombat(parameters.cid, combat7, parameters.var) 
end 
local function onCastSpell8(parameters) 
    doCombat(parameters.cid, combat8, parameters.var) 
end 
local function onCastSpell9(parameters) 
    doCombat(parameters.cid, combat9, parameters.var) 
end 
local function onCastSpell10(parameters) 
    doCombat(parameters.cid, combat10, parameters.var) 
end 
local function onCastSpell11(parameters) 
    doCombat(parameters.cid, combat11, parameters.var) 
end 
local function onCastSpell12(parameters) 
    doCombat(parameters.cid, combat12, parameters.var) 
end 
local function onCastSpell13(parameters) 
    doCombat(parameters.cid, combat13, parameters.var) 
end 
local function onCastSpell14(parameters) 
    doCombat(parameters.cid, combat14, parameters.var) 
end 
local function onCastSpell1t(parameters) 
    doCombat(parameters.cid, tcombat1, parameters.var) 
end 
local function onCastSpell2t(parameters) 
    doCombat(parameters.cid, tcombat2, parameters.var) 
end 
local function onCastSpell3t(parameters) 
    doCombat(parameters.cid, tcombat3, parameters.var) 
end 
local function onCastSpell4t(parameters) 
    doCombat(parameters.cid, tcombat4, parameters.var) 
end 
local function onCastSpell5t(parameters) 
    doCombat(parameters.cid, tcombat5, parameters.var) 
end 
local function onCastSpell6t(parameters) 
    doCombat(parameters.cid, tcombat6, parameters.var) 
end 
local function onCastSpell7t(parameters) 
    doCombat(parameters.cid, tcombat7, parameters.var) 
end 
local function onCastSpell8t(parameters) 
    doCombat(parameters.cid, tcombat8, parameters.var) 
end 
local function onCastSpell9t(parameters) 
    doCombat(parameters.cid, tcombat9, parameters.var) 
end 
local function onCastSpell10t(parameters) 
    doCombat(parameters.cid, tcombat10, parameters.var) 
end 
local function onCastSpell11t(parameters) 
    doCombat(parameters.cid, tcombat11, parameters.var) 
end 
local function onCastSpell12t(parameters) 
    doCombat(parameters.cid, tcombat12, parameters.var) 
end 
local function onCastSpell13t(parameters) 
    doCombat(parameters.cid, tcombat13, parameters.var) 
end 
local function onCastSpell14t(parameters) 
    doCombat(parameters.cid, tcombat14, parameters.var) 
end 
function onCastSpell(cid, var) 
local parameters = { cid = cid, var = var} 
addEvent(onCastSpell1, 100, parameters) 
addEvent(onCastSpell2, 200, parameters) 
addEvent(onCastSpell3, 300, parameters) 
addEvent(onCastSpell4, 400, parameters) 
addEvent(onCastSpell5, 500, parameters) 
addEvent(onCastSpell12, 700, parameters) 
addEvent(onCastSpell13, 800, parameters) 
addEvent(onCastSpell14, 900, parameters) 
addEvent(onCastSpell1t, 500, parameters) 
addEvent(onCastSpell2t, 600, parameters) 
addEvent(onCastSpell3t, 700, parameters) 
addEvent(onCastSpell4t, 800, parameters) 
addEvent(onCastSpell5t, 900, parameters) 
addEvent(onCastSpell6t, 1000, parameters) 
addEvent(onCastSpell7t, 1100, parameters) 
addEvent(onCastSpell8t, 1200, parameters) 
addEvent(onCastSpell9t, 1300, parameters) 
addEvent(onCastSpell10t, 1400, parameters) 
addEvent(onCastSpell11t, 1500, parameters) 
return TRUE
end