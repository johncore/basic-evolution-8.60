local combat1 = createCombatObject()
setCombatParam(combat1, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
setCombatParam(combat1, COMBAT_PARAM_EFFECT, CONST_ME_DRAWBLOOD)
setCombatParam(combat1, COMBAT_PARAM_DISTANCEEFFECT, CONST_ANI_ARROW)
setCombatFormula(combat1, COMBAT_FORMULA_SKILL, 0, -9, 0.7, -5)

local combat2 = createCombatObject()
setCombatParam(combat2, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
setCombatParam(combat2, COMBAT_PARAM_EFFECT, CONST_ME_DRAWBLOOD)
setCombatParam(combat2, COMBAT_PARAM_DISTANCEEFFECT, CONST_ANI_ARROW)
setCombatFormula(combat2, COMBAT_FORMULA_SKILL, 0, -9, 0.7, -5)

local combat3 = createCombatObject()
setCombatParam(combat3, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
setCombatParam(combat3, COMBAT_PARAM_EFFECT, CONST_ME_DRAWBLOOD)
setCombatParam(combat3, COMBAT_PARAM_DISTANCEEFFECT, CONST_ANI_ARROW)
setCombatFormula(combat3, COMBAT_FORMULA_SKILL, 0, -9, 0.7, -5)


local function onCastSpell1(p)
return doCombat(p.cid, combat1, p.var)
end

local function onCastSpell2(p)
return doCombat(p.cid, combat2, p.var)
end

local function onCastSpell3(p)
return doCombat(p.cid, combat3, p.var)
end

function onCastSpell(cid, var)

local p = { cid = cid, var = var}
addEvent(onCastSpell1, 100, p)
addEvent(onCastSpell2, 200, p)
addEvent(onCastSpell3, 300, p)
	return true
end