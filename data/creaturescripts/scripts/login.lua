local config = {
	useFragHandler = getBooleanFromString(getConfigValue('useFragHandler'))
}

function onLogin(cid)
	
local useFragHandler = getBooleanFromString(getConfigValue('useFragHandler'))
local loss = getConfigValue('deathLostPercent')

	if(loss ~= nil) then
		doPlayerSetLossPercent(cid, PLAYERLOSS_EXPERIENCE, loss * 10)
	end

	local lastLogin = getPlayerLastLoginSaved(cid)
	if(lastLogin > 0) then
	text = "Welcome!\n Visit our webpage for a complete information about the server.\n//Server Staff"
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, text)
	else
		doPlayerSendOutfitWindow(cid)
	end

	if getPlayerStorageValue(cid, 11551) == 0 then
    setPlayerPromotionLevel(cid, 0)
	end
	
	if(not isPlayerGhost(cid)) then
		doSendMagicEffect(getCreaturePosition(cid), CONST_ME_TELEPORT)
	end

	registerCreatureEvent(cid, "Mail")
	registerCreatureEvent(cid, "GuildMotd")
	registerCreatureEvent(cid, "Idle")
	registerCreatureEvent(cid, "SkullCheck")
	registerCreatureEvent(cid, "ReportBug")
	registerCreatureEvent(cid, "levelUp")
	registerCreatureEvent(cid, "onPrepareDeath")
	registerCreatureEvent(cid, "FirstItems")
	registerCreatureEvent(cid, "Arena")
	registerCreatureEvent(cid, "reward")
	registerCreatureEvent(cid, "mensaje")
	registerCreatureEvent(cid, "ArenaKill")
	registerCreatureEvent(cid, "demonOakLogout")
	registerCreatureEvent(cid, "demonOakDeath")
	registerCreatureEvent(cid, "tormentorportal")
	registerCreatureEvent(cid, "questLook")
	registerCreatureEvent(cid, "forever amulet")
    registerCreatureEvent(cid, "charge amulet")
	if(config.useFragHandler) then
		registerCreatureEvent(cid, "SkullCheck")
	end
	
    -- if he did not make full arena 1 he must start from zero
    if getPlayerStorageValue(cid, 42309) < 1 then
        for i = 42300, 42309 do
            setPlayerStorageValue(cid, i, 0)
        end
    end
    -- if he did not make full arena 2 he must start from zero
    if getPlayerStorageValue(cid, 42319) < 1 then
        for i = 42310, 42319 do
            setPlayerStorageValue(cid, i, 0)
        end
    end
    -- if he did not make full arena 3 he must start from zero
    if getPlayerStorageValue(cid, 42329) < 1 then
        for i = 42320, 42329 do
            setPlayerStorageValue(cid, i, 0)
        end
    end
    if getPlayerStorageValue(cid, 42355) == -1 then
    setPlayerStorageValue(cid, 42355, 0) -- did not arena level
    setPlayerStorageValue(cid, 42350, 0) -- time to kick 0
    setPlayerStorageValue(cid, 42352, 0) -- is not in arena
	end
	return true
end