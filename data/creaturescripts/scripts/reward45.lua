function onAdvance(cid, skill, oldlevel, newlevel)
       
        if(getPlayerStorageValue(cid, 99963) ~= 1 and skill == SKILL__LEVEL and newlevel >= 45) then
                doPlayerAddItem(cid, 2160, 5)
                setPlayerStorageValue(cid, 99963, 1)
                doPlayerSendTextMessage(cid, 22, "You have received 5 crystal coins because you reached level 45")
                end
        return TRUE
end