function onAdvance(cid, skill, oldlevel, newlevel)
local pPos = getPlayerPosition(cid)
          if skill == SKILL__LEVEL then
        doSendAnimatedText(pPos, "Level Up", 210)
        doSendMagicEffect(pPos, 28)    
        doCreatureAddHealth(cid, getCreatureMaxHealth(cid))
        doCreatureAddMana(cid, getCreatureMaxMana(cid))  
		end
return TRUE
end