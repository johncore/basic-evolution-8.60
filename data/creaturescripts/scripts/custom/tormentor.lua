local m = {
		["Warmaster"] = {
			time = 60, -- Seconds
			to = { x = 1360, y = 859, z = 7 }, -- Where Teleport Goes
			tp = { x = 1361, y = 861, z = 3 } -- Where Teleport Creates
		},
		["Tormentor"] = {
			time = 60, -- Seconds
			to = { x=703, y=712, z=10 }, -- Where Teleport Goes
			tp = { x = 711, y = 724, z = 10 } -- Where Teleport Creates
		}
	}

function onKill(cid, target)
	local monster = m[getCreatureName(target)]
		local function deleteTeleport()
			local teleport = getTileItemById(monster.tp, 1387)
			if(teleport.uid > 0) then
				doRemoveItem(teleport.uid)
				doSendMagicEffect(monster.tp, CONST_ME_POFF)
				doSendAnimatedText(monster.tp, "Closed", TEXTCOLOR_RED)
			end
			return true
		end
	if(isPlayer(target) == true) then
		return true
	elseif(not monster) then
		return true
	else
		doCreateTeleport(1387, monster.to, monster.tp)
		addEvent(deleteTeleport, monster.time * 1000)
		doSendMagicEffect(monster.tp, CONST_ME_ENERGYAREA)
		doCreatureSay(cid, "You have " .. monster.time .. " seconds to escape!", TALKTYPE_ORANGE_1)
	end
	return true
end