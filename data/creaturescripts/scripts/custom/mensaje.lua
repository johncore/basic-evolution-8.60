function onAdvance(cid, skill, oldlevel, newlevel)
        local name = getCreatureName(cid)
	if getPlayerStorageValue(cid, 10560) == -1 then
		if skill == SKILL__LEVEL and newlevel >= 200 then
			doPlayerSendTextMessage(cid, 22, "Congratulations " .. name .. ", your mana rune has been upgraded!")
			setPlayerStorageValue(cid, 10560, 1)
			doSendAnimatedText(getCreaturePosition(cid),"+ Upgrade",20)
		end
	end
	return TRUE
end 