function onLook(cid, thing, position, lookDistance)
local quests = {6076, 50013, 11000, 254200, 12900, 43345, 5050, 42381, 42361, 42371, 45865, 6000, 6001, 6002, 6003, 6004, 6005, 6006, 6007, 6008, 6009, 6010, 6011, 6012, 6013, 6014, 6015, 6016, 6017, 6018, 6019}
local completed = {}
	if isPlayer(thing.uid) then
		for i = 1, #quests do
			if getPlayerStorageValue(thing.uid, quests[i]) > 0 then
				table.insert(completed, 1)
			end
		end
		doPlayerSetSpecialDescription(thing.uid, (getPlayerSex(thing.uid) == 0 and ".\nShe" or ".\nHe") .. " has completed ".. #completed .. "/" .. #quests .. " quests")
		doPlayerSendTextMessage(cid, 27, getPlayerName(thing.uid) .. " has completed " .. #completed .. "/" .. #quests .. " quests.")
	end
return true
end