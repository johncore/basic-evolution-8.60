function onUse(cid, item, fromPosition, itemEx, toPosition)
local pushPos = {x=1038, y=1682, z=6}
   	if item.uid == 10001 then
		queststatus = getPlayerStorageValue(cid,19000)
   		if queststatus == -1 then
   			doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR, "You have found a present.")
   			doPlayerAddItem(cid,2523,1)
			doPlayerAddItem(cid,2128,1)
   			setPlayerStorageValue(cid,19000,1)
			doTeleportThing(cid, pushPos, 1)
   		else
   			doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR, "It is empty.")
			doTeleportThing(cid, pushPos, 1)
   		end
	end
   	return 1
end