function onUse(cid, item, fromPosition, itemEx, toPosition)
	if not isPlayer(itemEx.uid) then
		return false
	elseif((not(isKnight(itemEx.uid)) or getPlayerLevel(itemEx.uid) < 50) and getPlayerCustomFlagValue(itemEx.uid, PlayerCustomFlag_GamemasterPrivileges) == FALSE) then
		doCreatureSay(itemEx.uid, "Only knights of level 50 or above may use this rune.", TALKTYPE_ORANGE_1)
		return true
	else
		local l = getPlayerLevel(cid)
		doCreatureAddHealth(itemEx.uid, math.random(l >= 350 and 1300 or l >= 200 and 1100 or 700, l >= 350 and 1450 or l >= 200 and 1300 or 700))
		doSendMagicEffect(getThingPos(itemEx.uid), CONST_ME_MAGIC_BLUE)
		doCreatureSay(itemEx.uid, "Aaaah...", TALKTYPE_ORANGE_1)
		return true
	end
end