function onUse(cid, item, fromPosition, itemEx, toPosition)
	if not isPlayer(itemEx.uid) then
		return false
	elseif((not(isPaladin(itemEx.uid)) or getPlayerLevel(itemEx.uid) < 40) and getPlayerCustomFlagValue(itemEx.uid, PlayerCustomFlag_GamemasterPrivileges) == FALSE) then
		doCreatureSay(itemEx.uid, "Only paladins of level 40 or above may use this rune.", TALKTYPE_ORANGE_1)
		return true
	else
		local l = getPlayerLevel(cid)
		doCreatureAddMana(itemEx.uid, math.random(l >= 350 and 350 or l >= 200 and 250 or 150, l >= 350 and 500 or l >= 200 and 350 or 250))
		doCreatureAddHealth(itemEx.uid, math.random(l >= 350 and 150 or l >= 200 and 100 or 50, l >= 350 and 250 or l >= 200 and 150 or 100))
		doSendMagicEffect(getThingPos(itemEx.uid), CONST_ME_MAGIC_BLUE)
		doCreatureSay(itemEx.uid, "Aaaah...", TALKTYPE_ORANGE_1)
        doRemoveItem(item.uid, 1)
		return true
	end
end