function onUse(cid, item, fromPosition, itemEx, toPosition)
	if not isPlayer(itemEx.uid) then
		return false
	elseif((not(isSorcerer(cid) or isDruid(cid)) or getPlayerLevel(itemEx.uid) < 40) and getPlayerCustomFlagValue(itemEx.uid, PlayerCustomFlag_GamemasterPrivileges) == FALSE) then
		doCreatureSay(itemEx.uid, "Only sorcerers and druids of level 40 or above may use this rune.", TALKTYPE_ORANGE_1)
		return true
	else
		local l = getPlayerLevel(cid)
		doCreatureAddMana(itemEx.uid, math.random(l >= 350 and 450 or l >= 200 and 350 or 210, l >= 350 and 600 or l >= 200 and 430 or 310))
		doSendMagicEffect(getThingPos(itemEx.uid), CONST_ME_MAGIC_BLUE)
		doCreatureSay(itemEx.uid, "Aaaah...", TALKTYPE_ORANGE_1)
        doRemoveItem(item.uid, 1)
		return true
	end
end