function onUse(cid, item, fromPosition, itemEx, toPosition)
	if not isPlayer(itemEx.uid) then
		return false
	elseif((not(isPaladin(itemEx.uid)) or getPlayerLevel(itemEx.uid) < 40) and getPlayerCustomFlagValue(itemEx.uid, PlayerCustomFlag_GamemasterPrivileges) == FALSE) then
		doCreatureSay(itemEx.uid, "Only paladins of level 40 or above may use this rune.", TALKTYPE_ORANGE_1)
		return true
	else
		local l = getPlayerLevel(cid)
		doCreatureAddMana(itemEx.uid, math.random(l >= 350 and 450 or l >= 200 and 350 or 250, l >= 350 and 650 or l >= 200 and 450 or 350))
		doCreatureAddHealth(itemEx.uid, math.random(l >= 350 and 220 or l >= 200 and 150 or 100, l >= 350 and 325 or l >= 200 and 210 or 150))
		doSendMagicEffect(getThingPos(itemEx.uid), CONST_ME_MAGIC_BLUE)
		doCreatureSay(itemEx.uid, "Aaaah...", TALKTYPE_ORANGE_1)
		return true
	end
end