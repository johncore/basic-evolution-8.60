-- Mana Rune by diviinoo 
-- Players below 200
local MIN = 120
local MAX = 140

-- Players above 200
local MIN2 = 210
local MAX2 = 240

local exhaust = createConditionObject(CONDITION_EXHAUST)
setConditionParam(exhaust, CONDITION_PARAM_TICKS, (getConfigInfo('timeBetweenExActions') - 100))

function onUse(cid, item, fromPosition, itemEx, toPosition)
	if isPlayer(itemEx.uid) == FALSE then
		return FALSE
	end

	if hasCondition(cid, CONDITION_EXHAUST_HEAL) == TRUE then
		doPlayerSendDefaultCancel(cid, RETURNVALUE_YOUAREEXHAUSTED)
		return TRUE
	end

	if((not(isKnight(itemEx.uid)) or getPlayerLevel(itemEx.uid) < 80) and getPlayerCustomFlagValue(itemEx.uid, PlayerCustomFlag_GamemasterPrivileges) == FALSE) then
		doCreatureSay(itemEx.uid, "Only knights of level 80 or above may use this rune.", TALKTYPE_ORANGE_1)
		return TRUE
	end

	 if(getPlayerLevel(cid) <= 200) then
		doCreatureAddMana(itemEx.uid, math.random(MIN, MAX))
		doAddCondition(cid, exhaust)
		doSendMagicEffect(getThingPos(itemEx.uid), CONST_ME_MAGIC_BLUE)
		doCreatureSay(itemEx.uid, "Aaaah...", TALKTYPE_ORANGE_1)
        doRemoveItem(item.uid, 1)
		return TRUE
	end
	
	 if(getPlayerLevel(cid) >= 200) then
		doCreatureAddMana(itemEx.uid, math.random(MIN2, MAX2))
		doAddCondition(cid, exhaust)
		doSendMagicEffect(getThingPos(itemEx.uid), CONST_ME_MAGIC_BLUE)
		doCreatureSay(itemEx.uid, "Aaaah...", TALKTYPE_ORANGE_1)
        doRemoveItem(item.uid, 1)
		return TRUE
	end
end