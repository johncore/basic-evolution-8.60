function onUse(cid, item, fromPosition, itemEx, toPosition)
   	if item.uid == 8001 then
		queststatus = getPlayerStorageValue(cid,43345)
   		if queststatus == -1 then
   			doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR, "You have found a nightmare shield.")
   			doPlayerAddItem(cid,6391,1)
   			setPlayerStorageValue(cid,43345,1)
   		else
   			doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR, "It is empty.")
   		end
   	elseif item.uid == 8002 then
		queststatus = getPlayerStorageValue(cid,43345)
   		if queststatus == -1 then
   			doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR, "You have found a  emerald sword.")
   			doPlayerAddItem(cid,8930,1)
   			setPlayerStorageValue(cid,43345,1)
   		else
   			doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR, "It is empty.")
   		end
   	elseif item.uid == 8003 then
		queststatus = getPlayerStorageValue(cid,43345)
   		if queststatus == -1 then
   			doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR, "You have found a hellforged Axe.")
   			doPlayerAddItem(cid,8924,1)
   			setPlayerStorageValue(cid,43345,1)
   		else
   			doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR, "It is empty.")
		end
   	elseif item.uid == 8004 then
		queststatus = getPlayerStorageValue(cid,43345)
   		if queststatus == -1 then
   			doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR, "You have found a present.")
   			doPlayerAddItem(cid,6568,1)
   			setPlayerStorageValue(cid,43345,1)
   		else
   			doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR, "It is empty.")
   		end
	end
   	return 1
end
