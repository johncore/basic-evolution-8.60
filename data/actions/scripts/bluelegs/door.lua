local storage = 40586
function onUse(cid, item, fromPosition, itemEx, toPosition)
	if (item.actionid == 50016) then
		if (getPlayerStorageValue(cid, storage) == 1) then
			if getCreaturePosition(cid).y < toPosition.y then
				doTeleportThing(cid, {x=toPosition.x,y=toPosition.y+1,z=toPosition.z}, TRUE)
			else
				doTeleportThing(cid, {x=toPosition.x,y=toPosition.y-1,z=toPosition.z}, TRUE)
			end
			doCreatureSay(cid, "Congratulations, go recieve your reward!", TALKTYPE_ORANGE_1)
			doSendMagicEffect(getCreaturePosition(cid), CONST_ME_TELEPORT)
		else
			doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "You must defeat Koshei the Deathless!")
		end
		return true
	end
end