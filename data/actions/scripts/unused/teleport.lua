function onUse(cid, item, frompos, item2, topos)
	local pushPos = {x=1189, y=1024, z=6}
	local pushPos2 = {x=1026, y=982, z=7}
	local pushPos3 = {x=1035, y=986, z=7}
	
	if item.actionid == 4051 then
		doTeleportThing(cid, pushPos, 1)
		doSendMagicEffect(getCreaturePosition(cid), CONST_ME_ENERGYAREA)
		doSendAnimatedText(getCreaturePosition(cid), "Trainers!", TEXTCOLOR_DARKYELLOW)
	
	elseif item.actionid == 4052 then
		doTeleportThing(cid, pushPos2, 1)
		doSendMagicEffect(getCreaturePosition(cid), CONST_ME_ENERGYAREA)
		doSendAnimatedText(getCreaturePosition(cid), "Shop!", TEXTCOLOR_LIGHTBLUE)
		
	elseif item.actionid == 4053 then
		doTeleportThing(cid, pushPos3, 1)
		doSendMagicEffect(getCreaturePosition(cid), CONST_ME_ENERGYAREA)
		doSendAnimatedText(getCreaturePosition(cid), "Casino!", TEXTCOLOR_LIGHTBLUE)
	else
		doPlayerSendCancel(cid, "Something is wrong.")
	end
	end