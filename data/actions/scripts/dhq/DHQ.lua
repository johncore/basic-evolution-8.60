function onUse(cid, item, frompos, item2, topos)

	wall1 = {x=1227, y=1042, z=9, stackpos=1} --Position of the wall/stone
    getwall1 = getThingfromPos(wall1)

    if item.uid == 30016 and item.itemid == 1945 and getwall.itemid == 1355 then
        doRemoveItem(getwall1.uid,1)
        doTransformItem(item.uid,item.itemid+1)
    elseif item.uid == 30016 and item.itemid == 1946 then
        doCreateItem(1355,1,wall1)	
        doTransformItem(item.uid,item.itemid-1)        
    else
        doPlayerSendCancel(cid,"Sorry, not possible")
    end

    return 1
end
