function onUse(cid, item, frompos, item2, topos)
if (item.actionid == 10092 and getPlayerStorageValue(cid,11552) >= 1) then
            doCreatureSay(cid, "Welcome VIP Player!", TALKTYPE_ORANGE_1)
            pos = getPlayerPosition(cid)

            if pos.x == topos.x then
                if pos.y < topos.y then
                    pos.y = topos.y + 1
                else
                    pos.y = topos.y - 1
                end
            elseif pos.y == topos.y then
                if pos.x < topos.x then
                    pos.x = topos.x + 1
                else
                    pos.x = topos.x - 1
                end
            else
                doPlayerSendTextMessage(cid,22,'Please stand in front of the door.')
            return TRUE
            end

            doTeleportThing(cid,pos)
            doSendMagicEffect(topos,12)
        else
            doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "Sorry, but only VIP Players can pass here! Buy VIP on the WEB.")
        end
        return TRUE
end