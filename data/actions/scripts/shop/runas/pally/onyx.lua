local Cost = 200
local CompraId = 7365
function onUse(cid, item, frompos, item2, topos)
  if item.uid == 8010 and item.itemid == 1512 then
    if doPlayerRemoveMoney(cid, Cost) == 1 then
      doPlayerAddItem(cid, CompraId, 25)
      doCreatureSay(cid, "Thanks for buying!.", TALKTYPE_ORANGE_1)
      doSendMagicEffect(topos,12)
    else
      doPlayerSendCancel(cid, 'No tienes dinero suficiente.')
    end
    doTransformItem(item.uid, 1512)
  elseif item.itemid == 1512 then
    doTransformItem(item.uid, 1512)
  end
end
 