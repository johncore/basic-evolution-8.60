local config = {
        cost = 20000,
        rune_id = 2313,
        backpack_id = 2001
}
       
function onUse(cid, item, fromPosition, itemEx, toPosition)
        if doPlayerRemoveMoney(cid, config.cost) == TRUE then
                local name = getItemNameById(config.rune_id)
                local bp = doPlayerAddItem(cid, config.backpack_id, 1) -- Editing this will not do anything.
			doSendMagicEffect(getPlayerPosition(cid),12)
                        doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_RED, "You have purchased a backpack of ".. name .."s for ".. config.cost .." gold.")
        for i=1,20 do
                        doAddContainerItem(bp, config.rune_id, 50) -- You can edit this number, it will give shots per rune.
                end
                else
                        doPlayerSendCancel(cid, "You need ".. config.cost .." gold.")
                end
        return TRUE
end 