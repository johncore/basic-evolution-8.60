local config = {
        cost = 3500,
        compraid = 2306
}
function onUse(cid, item, fromPosition, itemEx, toPosition)
if doPlayerRemoveMoney(cid, config.cost) == TRUE then
    doPlayerAddItem(cid, config.compraid, 50)
    doSendMagicEffect(getPlayerPosition(cid),12)
    doPlayerSendTextMessage(cid,22,"Thanks for buying!.")
else
    doPlayerSendCancel(cid,"You don't have enough money.")
    doSendMagicEffect(getPlayerPosition(cid),2)
end
return TRUE
end