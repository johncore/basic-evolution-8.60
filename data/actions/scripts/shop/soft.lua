local softboots = { new = 6132, old = 10021, cost = 50000 }
function onUse(cid, item, fromPosition, itemEx, toPosition)
	if(getPlayerItemCount(cid, softboots.old) >= 1) then
		if(getPlayerMoney(cid) >= softboots.cost) then
			doPlayerRemoveItem(cid, softboots.old, 1)
			doPlayerRemoveMoney(cid, softboots.cost)
			doPlayerAddItem(cid, softboots.new)
			doSendMagicEffect(getCreaturePosition(cid), CONST_ME_GIFT_WRAPS)
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Your have successfully recharged your soft boots.")
		else
			doPlayerSendCancel(cid, "You must have atleast ".. softboots.cost .." gold coins to recharge your soft boots.")
			doSendAnimatedText(getCreaturePosition(cid), "$$$", TEXTCOLOR_WHITE)
			doSendMagicEffect(getCreaturePosition(cid), CONST_ME_POFF)
		end
	else
		doPlayerSendCancel(cid, "You do not have any pairs of worn soft boots to recharge.")
		doSendMagicEffect(getCreaturePosition(cid), CONST_ME_POFF)
	end
	return true
end