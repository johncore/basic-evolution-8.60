function onUse(cid, item, fromPosition, itemEx, toPosition)
    if getPlayerBlessing(cid, 1) or getPlayerBlessing(cid, 2) or getPlayerBlessing(cid, 3) then
            doSendMagicEffect(getCreaturePosition(cid), CONST_ME_POFF)
            doCreatureSay(cid, "You have already been blessed.", TALKTYPE_ORANGE_1)
    else
        if doPlayerRemoveMoney(cid, 30000) == TRUE then
            doPlayerAddBlessing(cid, 1)
            doPlayerAddBlessing(cid, 2)
            doPlayerAddBlessing(cid, 3)
            doSendMagicEffect(getCreaturePosition(cid), CONST_ME_HOLYAREA)
            doSendAnimatedText(getCreaturePosition(cid), "BLESSED!", TEXTCOLOR_RED)
            doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, "You have been blessed by the gods.")
        else
            doSendAnimatedText(getCreaturePosition(cid), "$$$", TEXTCOLOR_WHITE)
            doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You need atleast 30000 gold coins to be blessed.")
        end
    end
    return TRUE
end