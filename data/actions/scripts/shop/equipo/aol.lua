local config = {
        cost = 10000,
        compraid = 2173
}
function onUse(cid, item, fromPosition, itemEx, toPosition)
if doPlayerRemoveMoney(cid, config.cost) == TRUE then
    doPlayerAddItem(cid, config.compraid, 1)
    doSendMagicEffect(getPlayerPosition(cid),12)
    doPlayerSendTextMessage(cid,22,"Gracias por comprar!.")
else
    doPlayerSendCancel(cid,"You need 10,000 gold coins.")
    doSendMagicEffect(getPlayerPosition(cid),2)
end
return TRUE
end