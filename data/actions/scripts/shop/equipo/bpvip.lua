local config = {
        cost = 20,
        compraid = 7342
}
function onUse(cid, item, fromPosition, itemEx, toPosition)
if doPlayerRemoveMoney(cid, config.cost) == TRUE then
    doPlayerAddItem(cid, config.compraid, 1)
    doSendMagicEffect(getPlayerPosition(cid),12)
    doPlayerSendTextMessage(cid,22,"Gracias por comprar!.")
else
    doPlayerSendCancel(cid,"You don't have enough money.")
    doSendMagicEffect(getPlayerPosition(cid),2)
end
return TRUE
end