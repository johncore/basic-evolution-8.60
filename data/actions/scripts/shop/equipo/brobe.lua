local Equipo = 2656
local DineroId = 2160
function onUse(cid, item, frompos, item2, topos)
  if item.uid == 8005 and item.itemid == 1512 then
    if doPlayerRemoveitem(cid, Equipo, 1) == 1 then
      doPlayerAddItem(cid, DineroId, 1)
      doCreatureSay(cid, "Aqui esta tu dinero!.", TALKTYPE_ORANGE_1)
      doSendMagicEffect(topos,12)
    else
      doPlayerSendCancel(cid, 'No tienes este item.')
    end
    doTransformItem(item.uid, 1512)
  elseif item.itemid == 1512 then
    doTransformItem(item.uid, 1512)
  end
end
 