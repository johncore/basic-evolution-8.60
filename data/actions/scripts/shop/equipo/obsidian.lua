local Cost = 3000
local CompraId = 5908
function onUse(cid, item, frompos, item2, topos)
  if item.uid == 8303 and item.itemid == 1512 then
    if doPlayerRemoveMoney(cid, Cost) == 1 then
      doPlayerAddItem(cid, CompraId, 5)
      doCreatureSay(cid, "Gracias por comprar!.", TALKTYPE_ORANGE_1)
      doSendMagicEffect(topos,12)
    else
      doPlayerSendCancel(cid, 'No tienes dinero suficiente.')
    end
    doTransformItem(item.uid, 1512)
  elseif item.itemid == 1512 then
    doTransformItem(item.uid, 1512)
  end
end
 