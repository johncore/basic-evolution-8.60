local t = {
        redStorage = 15000,
        blueStorage = 15001,
        redFlag = 18000,
        blueFlag = 18001,
        redGoal = 19000,
        blueGoal = 19001,
        fromPos = {x = 987, y = 1365, z = 6},
        toPos = {x = 1033, y = 1411, z = 6}
}
local function getPlayers()
        local thisTable = {}
        for x = t.fromPos.x, t.toPos.x do
                for y = t.fromPos.y, t.toPos.y do
                        for z = t.fromPos.z, t.toPos.z do
                                local myPos = {x = x, y = y, z = z}
                                local player = getTopCreature(myPos).uid
                                if player > 0 then table.insert(thisTable, player) else table.remove(thisTable, player) end
                        end
                end
        end
        return {thisTable = thisTable}
end
local function repeatFlagOnPlayer(cid)
        local k = getThingPos(cid)
        local r = {
                {pos = {x = k.x + 2, y = k.y - 2, z = k.z}, delay = 300},
                {pos = {x = k.x + 2, y = k.y + 2, z = k.z}, delay = 300},
                {pos = {x = k.x - 2, y = k.y + 2, z = k.z}, delay = 300},
                {pos = {x = k.x - 2, y = k.y, z = k.z}, delay = 300},
                {pos = {x = k.x - 2, y = k.y - 2, z = k.z}, delay = 300},
                {pos = {x = k.x, y = k.y - 2, z = k.z}, delay = 300}
        }
        local effects = {27, 28, 29, 30}
        if isPlayer(cid) then
                if getPlayerStorageValue(cid, t.blueStorage) == 1 and getPlayerStorageValue(cid, t.redFlag) == 1 and getGlobalStorageValue(t.redFlag) == 1 then
                        for i = 1, 6 do
                                addEvent(doSendDistanceShoot, r[i].delay, r[i].pos, k, CONST_ME_FIREWORK_YELLOW)
                        end
                        for i = 1, 4 do
                                addEvent(doSendMagicEffect, 1000, getThingPos(cid), effects[i])
                        end
                        return addEvent(repeatFlagOnPlayer, 1000, cid)
                elseif getPlayerStorageValue(cid, t.redStorage) == 1 and getPlayerStorageValue(cid, t.blueFlag) == 1 and getGlobalStorageValue(t.blueFlag) == 1 then
                        for i = 1, 6 do
                                addEvent(doSendDistanceShoot, r[i].delay, r[i].pos, k, CONST_ME_FIREWORK_YELLOW)
                        end
                        for i = 1, 4 do
                                addEvent(doSendMagicEffect, 1000, getThingPos(cid), effects[i])
                        end
                        return addEvent(repeatFlagOnPlayer, 1000, cid)
                else
                        return false
                end
        else
                return false
        end
end
function onStepIn(cid, item, position, fromPosition, toPosition, lastPosition, actor)
        if isPlayer(cid) then
                if item.actionid == 6000 then
                        if getPlayerStorageValue(cid, t.redStorage) == 1 then
                                if getPlayerStorageValue(cid, t.blueFlag) == -1 and getGlobalStorageValue(t.blueFlag) == -1 then
                                        setPlayerStorageValue(cid, t.blueFlag, 1)
                                        setGlobalStorageValue(t.blueFlag, 1)
                                        repeatFlagOnPlayer(cid)
                                        doBroadcastMessage(getCreatureName(cid) .. " has stolen the Blue Flag!")
                                else
                                        doCreatureSay(cid, "The flag is not at home.", 19)
                                        doTeleportThing(cid, fromPosition, false)
                                end
                        else
                                doCreatureSay(cid, "You may only pick up the opposite team flag.", 19)
                                doTeleportThing(cid, fromPosition, false)
                        end
                elseif item.actionid == 6001 then
                        if getPlayerStorageValue(cid, t.blueStorage) == 1 then
                                if getPlayerStorageValue(cid, t.redFlag) == -1 and getGlobalStorageValue(t.redFlag) == -1 then
                                        setPlayerStorageValue(cid, t.redFlag, 1)
                                        setGlobalStorageValue(t.redFlag, 1)
                                        repeatFlagOnPlayer(cid)
                                        doBroadcastMessage(getCreatureName(cid) .. " has stolen the Red flag!")
                                else
                                        doCreatureSay(cid, "The flag is not at home.", 19)
                                        doTeleportThing(cid, fromPosition, false)
                                end
                        else
                                doCreatureSay(cid, "The flag is not at home.", 19)
                                doTeleportThing(cid, fromPosition, false)
                        end
                end
                if item.actionid == 6002 then
                        if getPlayerStorageValue(cid, t.redStorage) == 1 then
                                if getPlayerStorageValue(cid, t.blueFlag) == 1 and getGlobalStorageValue(t.blueFlag) == 1 then
                                        setGlobalStorageValue(t.blueFlag, -1)
                                        setPlayerStorageValue(cid, t.blueFlag, -1)
                                        setGlobalStorageValue(t.redGoal, getGlobalStorageValue(t.redGoal)+1)
                                        doBroadcastMessage("Current CTF Game Score:\nRed Team: " .. getGlobalStorageValue(t.redGoal) .. "\nBlue Team: " .. getGlobalStorageValue(t.blueGoal) .. "\nTen Scores to win!", 20)
                                        doBroadcastMessage(getCreatureName(cid) .. " has scored 1 point for the Red Team!")
                                else
                                        doCreatureSay(cid, "You dont have the flag.", 19)
                                        doTeleportThing(cid, fromPosition, false)
                                end
                        elseif getPlayerStorageValue(cid, t.blueStorage) == 1 then
                                if getPlayerStorageValue(cid, t.redFlag) == 1 and getGlobalStorageValue(t.redFlag) == 1 then
                                        setGlobalStorageValue(t.redFlag, -1)
                                        setPlayerStorageValue(cid, t.redFlag, -1)
                                        setGlobalStorageValue(t.blueGoal, getGlobalStorageValue(t.blueGoal)+1)
                                        doBroadcastMessage("Current CTF Game Score:\nRed Team: " .. getGlobalStorageValue(t.redGoal) .. "\nBlue Team: " .. getGlobalStorageValue(t.blueGoal) .. "\nTen Scores to win!", 20)
                                        doBroadcastMessage(getCreatureName(cid) .. " has scored 1 point for the Blue Team!")
                                else
                                        doCreatureSay(cid, "You dont have the flag.", 19)
                                        doTeleportThing(cid, fromPosition, false)
                                end
                        end
                        if getGlobalStorageValue(t.blueGoal) == 10 and getGlobalStorageValue(t.redGoal) < 10 then
                                for _, cid in ipairs(getPlayersOnline()) do
                                        if getPlayerStorageValue(cid, t.blueStorage) == 1 then
                                                setPlayerStorageValue(cid, t.blueStorage, -1)
                                                doPlayerAddItem(cid, 9020, math.random(1, 6))
                                        elseif getPlayerStorageValue(cid, t.redStorage) == 1 then
                                                setPlayerStorageValue(cid, t.redStorage, -1)
                                        end
                                        doRemoveCondition(cid, CONDITION_OUTFIT)
                                        setGlobalStorageValue(t.redGoal, 0)
                                        setGlobalStorageValue(t.blueGoal, 0)
                                end
                                local FF = getPlayers().thisTable
                                if #FF > 1 then
                                        for _, hh in ipairs(FF) do
                                                doTeleportThing(hh, getTownTemplePosition(getPlayerTown(hh)), true)
                                        end
                                end
                        elseif getGlobalStorageValue(t.redGoal) == 10 and getGlobalStorageValue(t.blueGoal) < 10 then
                                for _, cid in ipairs(getPlayersOnline()) do
                                        if getPlayerStorageValue(cid, t.redStorage) == 1 then
                                                setPlayerStorageValue(cid, t.redStorage, -1)
                                                doPlayerAddItem(cid, 9020, math.random(1, 6))
                                        elseif getPlayerStorageValue(cid, t.blueStorage) == 1 then
                                                setPlayerStorageValue(cid, t.blueStorage, -1)
                                        end
                                        doRemoveCondition(cid, CONDITION_OUTFIT)
                                        setGlobalStorageValue(t.redGoal, 0)
                                        setGlobalStorageValue(t.blueGoal, 0)
                                end
                                local F = getPlayers().thisTable
                                if #F > 1 then
                                        for _, h in ipairs(F) do
                                                doTeleportThing(h, getTownTemplePosition(getPlayerTown(h)), true)
                                        end
                                end
                        end
                end
        else
                doRemoveCreature(cid)
        end
        return true
end