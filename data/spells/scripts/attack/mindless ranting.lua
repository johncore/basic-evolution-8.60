-- SpellCreator generated.

-- =============== COMBAT VARS ===============
-- Areas/Combat for 1300ms
local combat13_Mort = createCombatObject()
setCombatParam(combat13_Mort, COMBAT_PARAM_EFFECT, CONST_ME_MORTAREA)
setCombatParam(combat13_Mort, COMBAT_PARAM_TYPE, COMBAT_DEATHDAMAGE)
setCombatArea(combat13_Mort,createCombatArea({{0, 0, 1, 0, 1, 0, 0},
{0, 1, 0, 0, 0, 1, 0},
{1, 0, 0, 0, 0, 0, 1},
{0, 0, 0, 2, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 1},
{0, 0, 0, 0, 0, 1, 0},
{0, 0, 0, 0, 1, 0, 0}}))
function getDmg_Mort(cid, level, maglevel)
	return (400)*-1,(500)*-1 
end
setCombatCallback(combat13_Mort, CALLBACK_PARAM_LEVELMAGICVALUE, "getDmg_Mort")

-- Areas/Combat for 1200ms
local combat12_Mort = createCombatObject()
setCombatParam(combat12_Mort, COMBAT_PARAM_EFFECT, CONST_ME_MORTAREA)
setCombatParam(combat12_Mort, COMBAT_PARAM_TYPE, COMBAT_DEATHDAMAGE)
setCombatArea(combat12_Mort,createCombatArea({{0, 0, 0, 0, 1, 0, 0, 0, 0},
{0, 0, 0, 1, 0, 1, 0, 0, 0},
{0, 0, 1, 0, 0, 0, 1, 0, 0},
{0, 1, 0, 0, 0, 0, 0, 1, 0},
{1, 0, 0, 0, 2, 0, 0, 0, 1},
{0, 1, 0, 0, 0, 0, 0, 1, 0},
{0, 0, 1, 0, 0, 0, 1, 0, 0},
{0, 0, 0, 1, 0, 1, 0, 0, 0},
{0, 0, 0, 0, 1, 0, 0, 0, 0}}))
function getDmg_Mort(cid, level, maglevel)
	return (400)*-1,(500)*-1 
end
setCombatCallback(combat12_Mort, CALLBACK_PARAM_LEVELMAGICVALUE, "getDmg_Mort")

-- Areas/Combat for 1100ms
local combat11_Mort = createCombatObject()
setCombatParam(combat11_Mort, COMBAT_PARAM_EFFECT, CONST_ME_MORTAREA)
setCombatParam(combat11_Mort, COMBAT_PARAM_TYPE, COMBAT_DEATHDAMAGE)
setCombatArea(combat11_Mort,createCombatArea({{0, 0, 0, 1, 0, 0, 0},
{0, 0, 1, 0, 1, 0, 0},
{0, 1, 0, 0, 0, 1, 0},
{1, 0, 0, 2, 0, 0, 1},
{0, 1, 0, 0, 0, 1, 0},
{0, 0, 1, 0, 1, 0, 0},
{0, 0, 0, 1, 0, 0, 0}}))
function getDmg_Mort(cid, level, maglevel)
	return (400)*-1,(500)*-1 
end
setCombatCallback(combat11_Mort, CALLBACK_PARAM_LEVELMAGICVALUE, "getDmg_Mort")

-- Areas/Combat for 1000ms
local combat10_Mort = createCombatObject()
setCombatParam(combat10_Mort, COMBAT_PARAM_EFFECT, CONST_ME_MORTAREA)
setCombatParam(combat10_Mort, COMBAT_PARAM_TYPE, COMBAT_DEATHDAMAGE)
setCombatArea(combat10_Mort,createCombatArea({{0, 0, 1, 0, 0},
{0, 1, 0, 1, 0},
{1, 0, 2, 0, 1},
{0, 1, 0, 1, 0},
{0, 0, 1, 0, 0}}))
function getDmg_Mort(cid, level, maglevel)
	return (400)*-1,(500)*-1 
end
setCombatCallback(combat10_Mort, CALLBACK_PARAM_LEVELMAGICVALUE, "getDmg_Mort")

-- Areas/Combat for 900ms
local combat9_Mort = createCombatObject()
setCombatParam(combat9_Mort, COMBAT_PARAM_EFFECT, CONST_ME_MORTAREA)
setCombatParam(combat9_Mort, COMBAT_PARAM_TYPE, COMBAT_DEATHDAMAGE)
setCombatArea(combat9_Mort,createCombatArea({{0, 1, 0},
{1, 2, 1},
{0, 1, 0}}))
function getDmg_Mort(cid, level, maglevel)
	return (400)*-1,(500)*-1 
end
setCombatCallback(combat9_Mort, CALLBACK_PARAM_LEVELMAGICVALUE, "getDmg_Mort")

-- Areas/Combat for 800ms
local combat8_Flam = createCombatObject()
setCombatParam(combat8_Flam, COMBAT_PARAM_EFFECT, CONST_ME_FIREAREA)
setCombatParam(combat8_Flam, COMBAT_PARAM_TYPE, COMBAT_FIREDAMAGE)
setCombatArea(combat8_Flam,createCombatArea({{0, 0, 1, 0, 1, 0, 0},
{0, 1, 0, 0, 0, 1, 0},
{1, 0, 0, 0, 0, 0, 1},
{0, 0, 0, 2, 0, 0, 0},
{1, 0, 0, 0, 0, 0, 1},
{0, 1, 0, 0, 0, 1, 0},
{0, 0, 1, 0, 1, 0, 0}}))
function getDmg_Flam(cid, level, maglevel)
	return (600)*-1,(700)*-1 
end
setCombatCallback(combat8_Flam, CALLBACK_PARAM_LEVELMAGICVALUE, "getDmg_Flam")

-- Areas/Combat for 600ms
local combat6_Flam = createCombatObject()
setCombatParam(combat6_Flam, COMBAT_PARAM_EFFECT, CONST_ME_FIREAREA)
setCombatParam(combat6_Flam, COMBAT_PARAM_TYPE, COMBAT_FIREDAMAGE)
setCombatArea(combat6_Flam,createCombatArea({{0, 0, 0, 1, 0, 0, 0},
{0, 0, 1, 0, 1, 0, 0},
{0, 1, 0, 0, 0, 1, 0},
{1, 0, 0, 2, 0, 0, 1},
{0, 1, 0, 0, 0, 1, 0},
{0, 0, 1, 0, 1, 0, 0},
{0, 0, 0, 1, 0, 0, 0}}))
function getDmg_Flam(cid, level, maglevel)
	return (600)*-1,(700)*-1 
end
setCombatCallback(combat6_Flam, CALLBACK_PARAM_LEVELMAGICVALUE, "getDmg_Flam")

-- Areas/Combat for 400ms
local combat4_Flam = createCombatObject()
setCombatParam(combat4_Flam, COMBAT_PARAM_EFFECT, CONST_ME_FIREAREA)
setCombatParam(combat4_Flam, COMBAT_PARAM_TYPE, COMBAT_FIREDAMAGE)
setCombatArea(combat4_Flam,createCombatArea({{0, 0, 0, 0, 1, 0, 0},
{0, 0, 0, 1, 0, 0, 0},
{1, 0, 1, 0, 1, 0, 0},
{0, 1, 0, 2, 0, 1, 0},
{0, 0, 1, 0, 1, 0, 1},
{0, 0, 0, 1, 0, 0, 0},
{0, 0, 1, 0, 0, 0, 0}}))
function getDmg_Flam(cid, level, maglevel)
	return (600)*-1,(700)*-1 
end
setCombatCallback(combat4_Flam, CALLBACK_PARAM_LEVELMAGICVALUE, "getDmg_Flam")

-- Areas/Combat for 200ms
local combat2_Flam = createCombatObject()
setCombatParam(combat2_Flam, COMBAT_PARAM_EFFECT, CONST_ME_FIREAREA)
setCombatParam(combat2_Flam, COMBAT_PARAM_TYPE, COMBAT_FIREDAMAGE)
setCombatArea(combat2_Flam,createCombatArea({{0, 0, 0, 0, 0, 1, 0},
{1, 0, 0, 0, 1, 0, 0},
{0, 1, 0, 1, 0, 0, 0},
{0, 0, 1, 2, 1, 0, 0},
{0, 0, 0, 1, 0, 1, 0},
{0, 0, 1, 0, 0, 0, 1},
{0, 1, 0, 0, 0, 0, 0}}))
function getDmg_Flam(cid, level, maglevel)
	return (600)*-1,(700)*-1 
end
setCombatCallback(combat2_Flam, CALLBACK_PARAM_LEVELMAGICVALUE, "getDmg_Flam")

-- Areas/Combat for 0ms
local combat0_Flam = createCombatObject()
setCombatParam(combat0_Flam, COMBAT_PARAM_EFFECT, CONST_ME_FIREAREA)
setCombatParam(combat0_Flam, COMBAT_PARAM_TYPE, COMBAT_FIREDAMAGE)
setCombatArea(combat0_Flam,createCombatArea({{1, 0, 0, 0, 0, 0, 1},
{0, 1, 0, 0, 0, 1, 0},
{0, 0, 1, 0, 1, 0, 0},
{0, 0, 0, 2, 0, 0, 0},
{0, 0, 1, 0, 1, 0, 0},
{0, 1, 0, 0, 0, 1, 0},
{1, 0, 0, 0, 0, 0, 1}}))
function getDmg_Flam(cid, level, maglevel)
	return (600)*-1,(700)*-1 
end
setCombatCallback(combat0_Flam, CALLBACK_PARAM_LEVELMAGICVALUE, "getDmg_Flam")

-- =============== CORE FUNCTIONS ===============
function RunPart(c,cid,var,dirList,dirEmitPos) -- Part
	if (isCreature(cid)) then
		doCombat(cid, c, var)
		if (dirList ~= nil) then -- Emit distance effects
		local i = 2;
			while (i < #dirList) do
				doSendDistanceShoot(dirEmitPos,{x=dirEmitPos.x-dirList[i],y=dirEmitPos.y-dirList[i+1],z=dirEmitPos.z},dirList[1])
				i = i + 2
			end		
		end
	end
end

function onCastSpell(cid, var)
	local startPos = getCreaturePosition(cid)
	addEvent(RunPart,1300,combat13_Mort,cid,var)
	addEvent(RunPart,1200,combat12_Mort,cid,var)
	addEvent(RunPart,1100,combat11_Mort,cid,var)
	addEvent(RunPart,1000,combat10_Mort,cid,var)
	addEvent(RunPart,900,combat9_Mort,cid,var)
	addEvent(RunPart,800,combat8_Flam,cid,var)
	addEvent(RunPart,600,combat6_Flam,cid,var)
	addEvent(RunPart,400,combat4_Flam,cid,var)
	addEvent(RunPart,200,combat2_Flam,cid,var)
	RunPart(combat0_Flam,cid,var)
	return true
end
