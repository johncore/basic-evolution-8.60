-- SpellCreator generated.

-- =============== COMBAT VARS ===============
-- Areas/Combat for 0ms
local combat0_Spika = createCombatObject()
setCombatParam(combat0_Spika, COMBAT_PARAM_EFFECT, CONST_ME_ICEATTACK)
setCombatParam(combat0_Spika, COMBAT_PARAM_TYPE, COMBAT_ICEDAMAGE)
setCombatArea(combat0_Spika,createCombatArea({{0, 1, 0},
{1, 2, 1},
{0, 1, 0}}))
function getDmg_Spika(cid, level, maglevel)
	return (500)*-1,(600)*-1 
end
setCombatCallback(combat0_Spika, CALLBACK_PARAM_LEVELMAGICVALUE, "getDmg_Spika")

-- Areas/Combat for 1200ms
local combat12_Priest_Wrath = createCombatObject()
setCombatParam(combat12_Priest_Wrath, COMBAT_PARAM_EFFECT, CONST_ME_HOLYAREA)
setCombatParam(combat12_Priest_Wrath, COMBAT_PARAM_TYPE, COMBAT_HOLYDAMAGE)
setCombatArea(combat12_Priest_Wrath,createCombatArea({{0, 0, 0, 0, 1, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0},
{1, 0, 0, 0, 2, 0, 0, 0, 1},
{0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 1, 0, 0, 0, 0}}))
function getDmg_Priest_Wrath(cid, level, maglevel)
	return (600)*-1,(700)*-1 
end
setCombatCallback(combat12_Priest_Wrath, CALLBACK_PARAM_LEVELMAGICVALUE, "getDmg_Priest_Wrath")

-- Areas/Combat for 1000ms
local combat10_Shimmer = createCombatObject()
setCombatParam(combat10_Shimmer, COMBAT_PARAM_EFFECT, CONST_ME_FIREWORK_BLUE)
setCombatParam(combat10_Shimmer, COMBAT_PARAM_TYPE, COMBAT_MANADRAIN)
setCombatArea(combat10_Shimmer,createCombatArea({{0, 0, 0, 0, 1, 0, 0, 0, 0},
{0, 0, 0, 1, 0, 1, 0, 0, 0},
{0, 0, 1, 0, 0, 0, 1, 0, 0},
{0, 1, 0, 0, 0, 0, 0, 1, 0},
{1, 0, 0, 0, 2, 0, 0, 0, 1},
{0, 1, 0, 0, 0, 0, 0, 1, 0},
{0, 0, 1, 0, 0, 0, 1, 0, 0},
{0, 0, 0, 1, 0, 1, 0, 0, 0},
{0, 0, 0, 0, 1, 0, 0, 0, 0}}))
function getDmg_Shimmer(cid, level, maglevel)
	return (100)*-1,(200)*-1 
end
setCombatCallback(combat10_Shimmer, CALLBACK_PARAM_LEVELMAGICVALUE, "getDmg_Shimmer")

-- Areas/Combat for 900ms
local combat9_Shimmer = createCombatObject()
setCombatParam(combat9_Shimmer, COMBAT_PARAM_EFFECT, CONST_ME_FIREWORK_BLUE)
setCombatParam(combat9_Shimmer, COMBAT_PARAM_TYPE, COMBAT_MANADRAIN)
setCombatArea(combat9_Shimmer,createCombatArea({{0, 0, 0, 1, 0, 0, 0},
{0, 0, 1, 0, 1, 0, 0},
{0, 1, 0, 0, 0, 1, 0},
{1, 0, 0, 2, 0, 0, 1},
{0, 1, 0, 0, 0, 1, 0},
{0, 0, 1, 0, 1, 0, 0},
{0, 0, 0, 1, 0, 0, 0}}))
function getDmg_Shimmer(cid, level, maglevel)
	return (100)*-1,(200)*-1 
end
setCombatCallback(combat9_Shimmer, CALLBACK_PARAM_LEVELMAGICVALUE, "getDmg_Shimmer")

-- Areas/Combat for 800ms
local combat8_Shimmer = createCombatObject()
setCombatParam(combat8_Shimmer, COMBAT_PARAM_EFFECT, CONST_ME_FIREWORK_BLUE)
setCombatParam(combat8_Shimmer, COMBAT_PARAM_TYPE, COMBAT_MANADRAIN)
setCombatArea(combat8_Shimmer,createCombatArea({{0, 0, 1, 0, 0},
{0, 1, 0, 1, 0},
{1, 0, 2, 0, 1},
{0, 1, 0, 1, 0},
{0, 0, 1, 0, 0}}))
function getDmg_Shimmer(cid, level, maglevel)
	return (100)*-1,(200)*-1 
end
setCombatCallback(combat8_Shimmer, CALLBACK_PARAM_LEVELMAGICVALUE, "getDmg_Shimmer")

-- Areas/Combat for 700ms
local combat7_Shimmer = createCombatObject()
setCombatParam(combat7_Shimmer, COMBAT_PARAM_EFFECT, CONST_ME_FIREWORK_BLUE)
setCombatParam(combat7_Shimmer, COMBAT_PARAM_TYPE, COMBAT_MANADRAIN)
setCombatArea(combat7_Shimmer,createCombatArea({{0, 1, 0},
{1, 2, 1},
{0, 1, 0}}))
function getDmg_Shimmer(cid, level, maglevel)
	return (100)*-1,(200)*-1 
end
setCombatCallback(combat7_Shimmer, CALLBACK_PARAM_LEVELMAGICVALUE, "getDmg_Shimmer")

-- Areas/Combat for 300ms
local combat3_Spika = createCombatObject()
setCombatParam(combat3_Spika, COMBAT_PARAM_EFFECT, CONST_ME_ICEATTACK)
setCombatParam(combat3_Spika, COMBAT_PARAM_TYPE, COMBAT_ICEDAMAGE)
setCombatArea(combat3_Spika,createCombatArea({{0, 0, 0, 0, 1, 0, 0, 0, 0},
{0, 0, 0, 1, 0, 1, 0, 0, 0},
{0, 0, 1, 0, 0, 0, 1, 0, 0},
{0, 1, 0, 0, 0, 0, 0, 1, 0},
{1, 0, 0, 0, 2, 0, 0, 0, 1},
{0, 1, 0, 0, 0, 0, 0, 1, 0},
{0, 0, 1, 0, 0, 0, 1, 0, 0},
{0, 0, 0, 1, 0, 1, 0, 0, 0},
{0, 0, 0, 0, 1, 0, 0, 0, 0}}))
function getDmg_Spika(cid, level, maglevel)
	return (500)*-1,(600)*-1 
end
setCombatCallback(combat3_Spika, CALLBACK_PARAM_LEVELMAGICVALUE, "getDmg_Spika")

-- Areas/Combat for 200ms
local combat2_Spika = createCombatObject()
setCombatParam(combat2_Spika, COMBAT_PARAM_EFFECT, CONST_ME_ICEATTACK)
setCombatParam(combat2_Spika, COMBAT_PARAM_TYPE, COMBAT_ICEDAMAGE)
setCombatArea(combat2_Spika,createCombatArea({{0, 0, 0, 1, 0, 0, 0},
{0, 0, 1, 0, 1, 0, 0},
{0, 1, 0, 0, 0, 1, 0},
{1, 0, 0, 2, 0, 0, 1},
{0, 1, 0, 0, 0, 1, 0},
{0, 0, 1, 0, 1, 0, 0},
{0, 0, 0, 1, 0, 0, 0}}))
function getDmg_Spika(cid, level, maglevel)
	return (500)*-1,(600)*-1 
end
setCombatCallback(combat2_Spika, CALLBACK_PARAM_LEVELMAGICVALUE, "getDmg_Spika")

-- Areas/Combat for 100ms
local combat1_Spika = createCombatObject()
setCombatParam(combat1_Spika, COMBAT_PARAM_EFFECT, CONST_ME_ICEATTACK)
setCombatParam(combat1_Spika, COMBAT_PARAM_TYPE, COMBAT_ICEDAMAGE)
setCombatArea(combat1_Spika,createCombatArea({{0, 0, 1, 0, 0},
{0, 1, 0, 1, 0},
{1, 0, 2, 0, 1},
{0, 1, 0, 1, 0},
{0, 0, 1, 0, 0}}))
function getDmg_Spika(cid, level, maglevel)
	return (500)*-1,(600)*-1 
end
setCombatCallback(combat1_Spika, CALLBACK_PARAM_LEVELMAGICVALUE, "getDmg_Spika")

-- =============== CORE FUNCTIONS ===============
function RunPart(c,cid,var,dirList,dirEmitPos) -- Part
	if (isCreature(cid)) then
		doCombat(cid, c, var)
		if (dirList ~= nil) then -- Emit distance effects
		local i = 2;
			while (i < #dirList) do
				doSendDistanceShoot(dirEmitPos,{x=dirEmitPos.x-dirList[i],y=dirEmitPos.y-dirList[i+1],z=dirEmitPos.z},dirList[1])
				i = i + 2
			end		
		end
	end
end

function onCastSpell(cid, var)
	local startPos = getCreaturePosition(cid)
	RunPart(combat0_Spika,cid,var)
	addEvent(RunPart,1200,combat12_Priest_Wrath,cid,var)
	addEvent(RunPart,1000,combat10_Shimmer,cid,var)
	addEvent(RunPart,900,combat9_Shimmer,cid,var)
	addEvent(RunPart,800,combat8_Shimmer,cid,var)
	addEvent(RunPart,700,combat7_Shimmer,cid,var)
	addEvent(RunPart,300,combat3_Spika,cid,var)
	addEvent(RunPart,200,combat2_Spika,cid,var)
	addEvent(RunPart,100,combat1_Spika,cid,var)
	return true
end
