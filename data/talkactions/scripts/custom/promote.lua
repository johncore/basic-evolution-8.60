----- Antigua RPG --- Diviinoo ----
function onSay(cid, words, param)
	if(getPlayerItemCount(cid, 5785) > 0) then
		    setPlayerPromotionLevel(cid, 1)
			doSendMagicEffect(getCreaturePosition(cid),14)
			doSendAnimatedText(getCreaturePosition(cid), "PROMOTED!" ,49)
			setPlayerStorageValue(cid,11551,1)
            doPlayerRemoveItem(cid, 5785, 1)
	 else
            doPlayerSendCancel(cid, "You dont have a promotion medal!")
            doSendMagicEffect(getPlayerPosition(cid), CONST_ME_POFF)
        end
    return TRUE
end