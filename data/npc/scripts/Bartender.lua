local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

function onCreatureAppear(cid)              npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid)           npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg)  npcHandler:onCreatureSay(cid, type, msg) end
function onThink()                      npcHandler:onThink() end

local shopModule = ShopModule:new()
npcHandler:addModule(shopModule)

shopModule:addBuyableItem({'lemonade', 'lemondrink'},                   2006, 10,   5,  'lemonade')
shopModule:addBuyableItem({'fruit juice', 'fruitjuice'},                    2006, 10,   21, 'fruit juice')
shopModule:addBuyableItem({'beer', 'booze'},                    2006, 10,   3,  'beer')
shopModule:addBuyableItem({'milk'},                     2006, 10,   6,  'milk')
shopModule:addBuyableItem({'coconut'},                  2006, 10,   14, 'coconut milk')
shopModule:addBuyableItem({'wine'},                     2006, 10,   15, 'wine')
shopModule:addBuyableItem({'rum'},                  2006, 10,   27, 'rum')

npcHandler:addModule(FocusModule:new())