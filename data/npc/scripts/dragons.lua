-- Antigua RPG | diviinoo
local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

-- OTServ event handling functions start
function onCreatureAppear(cid)              npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid)           npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg)  npcHandler:onCreatureSay(cid, type, msg) end
function onThink()                      npcHandler:onThink() end
-- OTServ event handling functions end

function creatureSayCallback(cid, type, msg)
    if (not npcHandler:isFocused(cid)) then
        return false
    end
        if msgcontains(msg, 'green dragon leather') then
            npcHandler:say('You got the 100 green dragon leathers?', cid)
            talk_state = 1
        elseif msgcontains(msg, 'yes') and talk_state == 1 then
            local rewardexp, itemid, amount, storage = 32000, 5877, 100, 35710
			if getPlayerStorageValue(cid, storage) ~= 1 then
				if getPlayerItemCount(cid, itemid) >= amount then
				if doPlayerRemoveItem(cid, itemid, amount) == true then
						doPlayerAddExperience(cid, rewardexp)
						doPlayerAddOutfit(cid, 131, 1)
						doPlayerAddOutfit(cid, 139, 1)
						doPlayerAddOutfit(cid, 131, 2)
						doPlayerAddOutfit(cid, 139, 2)
						setPlayerStorageValue(cid,storage,1)
						npcHandler:say("Thanks! Here are your "..rewardexp.." exp and your addon for beign a good warrior.", cid)
						talk_state = 0
					end	
				else 
					doPlayerSendCancel(cid, "You don't have enough dragon leathers!")
				end
			else 
				doPlayerSendCancel(cid, "You have already done this quest!")
			end
------------------------------------------------ confirm no ------------------------------------------------
        elseif msgcontains(msg, 'no') and (talk_state >= 1 and talk_state <= 3) then
            npcHandler:say('Okay thanks.', cid)
            talk_state = 0
        end
		return true
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())