function onThink(interval, lastExecution)

local texts =
{
		-- Nova
        ["Teleports"] = {{x=1001, y=995, z=7}, CONST_ME_FIREWORK_YELLOW, TEXTCOLOR_GREEN},
        ["Quests"] = {{x=1001, y=996, z=7}, CONST_ME_FIREWORK_RED, TEXTCOLOR_GREEN},
        ["Trainers"] = {{x=1001, y=994, z=7}, CONST_ME_YELLOW_RINGS, TEXTCOLOR_DARKYELLOW},
		["Shops"] = {{x=1005, y=995, z=7}, CONST_ME_ENERGYAREA, TEXTCOLOR_LIGHTBLUE},
		["Depot"] = {{x=1005, y=996, z=7}, CONST_ME_CRAPS, TEXTCOLOR_BROWN},
		["Games!"] = {{x=1005, y=994, z=7}, CONST_ME_MAGIC_BLUE, TEXTCOLOR_GREEN},
		["Emporia    "] = {{x=902, y=1091, z=7}, CONST_ME_ENERGYAREA, TEXTCOLOR_LIGHTBLUE},
		["Coast"] = {{x=1001, y=1001, z=7}, CONST_ME_WATERSPLASH, TEXTCOLOR_LIGHTBLUE},
		["Island"] = {{x=1006, y=1001, z=7}, CONST_ME_FIREWORK_BLUE, TEXTCOLOR_LIGHTBLUE},
		-- Temple signs
		["Emporia"] = {{x=1091, y=943, z=6}, CONST_ME_ENERGYAREA, TEXTCOLOR_LIGHTBLUE},
		["Lumina "] = {{x=1091, y=954, z=6}, CONST_ME_ENERGYAREA, TEXTCOLOR_LIGHTBLUE},
		["Temple  "] = {{x=1026, y=995, z=7}, CONST_ME_ENERGYAREA, TEXTCOLOR_LIGHTBLUE},
		["Emporia   "] = {{x=1091, y=943, z=7}, CONST_ME_ENERGYAREA, TEXTCOLOR_LIGHTBLUE},
		["Teleports "] = {{x=1078, y=1169, z=7}, CONST_ME_MAGIC_BLUE, TEXTCOLOR_GREEN},
        ["Quests "] = {{x=1082, y=1169, z=7}, CONST_ME_MAGIC_BLUE, TEXTCOLOR_GREEN},
		["Lumina    "] = {{x=1091, y=954, z=7}, CONST_ME_ENERGYAREA, TEXTCOLOR_LIGHTBLUE}
}

        for text, param in pairs(texts) do
                doSendAnimatedText(param[1], text, param[3])
                doSendMagicEffect(param[1], param[2])
        end
        return TRUE
end