local messages = {
	"Please report bugs and missing creatures/npcs/quests at our forums with images and description.",
	"Follow the rules to avoid a banishment.",
	"You have the possibility to donate with a credit card or through phone at delyria.no-ip.org",
	"Visit the official homepage on a regular basis to have an overview of the latest news.",
	"We aren't the ones creating the wars and quest services, you are! Invite friends, it only makes the server better.",
	"Report all bugs you find or give us suggestions at --> delyria.no-ip.org",
	"Never trust a in-game friend by 100%. Sometimes the appearances are deceiving.",
	"Invite friends to play, make a guild, make money, find rare items, become a legend.",
	"Your account has been banned! This is a joke but this would be real if you use third party software like cavebots!",
	"If you die, you loose items, skills and experience. But if you don't hunt, you'll never get them back",
	"When someone threatens to kill you if you don't give them your armor, give them nothing, as they might kill you anyway.",
	"Don't throw empty backpacks on the floor, that's what trashcans are for!",
	"Please think twice before you kill a dog. They just want to be your friend!"
}

local i = 0
function onThink(interval, lastExecution)
local message = messages[(i % #messages) + 1]
    doBroadcastMessage("Information: " .. message .. "", MESSAGE_STATUS_CONSOLE_ORANGE)
    i = i + 1
    return TRUE
end