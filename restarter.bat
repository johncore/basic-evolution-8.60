echo off
title OTServer Restarter
color 80
cls
echo :::::::::::::::::::::::::::::::::::::::::::::::::::
echo ::             OTServer Restarter                ::
echo :::::::::::::::::::::::::::::::::::::::::::::::::::
 
:controllerini
theforgottenserver.exe
echo :::::::::::::::::::::::::::::::::::::::::::::::::::
echo ::         Your OTServer is restarting...        ::
echo :::::::::::::::::::::::::::::::::::::::::::::::::::
goto :controllerini